$(function () {
    setTimeout("$('#get-button').click()", 100);
    // GET/READ
    $("#create-input").click(function () {
        $("#create-input").animate({ width: "100%", height: "30%" });
        $("#allMemo").animate({ "margin-top": "1%" });

    })

    $("#create-input").mouseleave(function () {
        $("#create-input").animate({ width: "15%", height: "5%" });
        $("#allMemo").animate({ "margin-top": "14%" });
    })

    $("#create-title").click(function () {
        $("#create-title").animate({ width: "50%" });

    })

    $("#create-title").mouseleave(function () {
        $("#create-title").animate({ width: "15%" });
    })

    $('#get-button').on('click', function () {      //mostra tutti i memo
        $.ajax({
            url: '/memo',
            contentType: 'application/json',
            success: function (response) {
                var table = $('#allMemo');

                table.html('');

                response.products.forEach(function (product) {
                    table.append('\
                        <div class="memo">\
                            <input type="text" class="id" value="'+ product.id + '">\
                            <button class="update-button" data-toggle="tooltip" data-placement="top" title="salva modifiche">✏️</button>\
                            <button class="delete-button"  data-toggle="tooltip" data-placement="top" title="rimuovi promemoria">x</button>\
                            <textarea type="text" class="tit" >'+ product.title + '</textarea>\
                            <textarea type="text" class="name"  " >' + product.name + '</textarea>\
                        </div>\
                    ');
                    console.log(product.title + "  shanuiz")
                });
            }
        });
    });

    // CREATE/POST


    $('#create-form').on('submit', function (event) {   //aggiunge un memo
        event.preventDefault();
        var createTitle = $('#create-title');
        var createInput = $('#create-input');
        let acc=$('#accoun');
        console.log("cien"+acc.text())
        console.log("feomemno" + createTitle.val());


        $.ajax({
            url: '/memo',
            method: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({ name: createInput.val(), title: createTitle.val(),accoun:acc.text() }),
            success: function (response) {
                
                console.log(response);
                var table = $('#allMemo');
                setTimeout(appendi(), 100);
                function appendi() {
                   
                    table.append('\
                        <div class="memo">\
                            <input type="text" class="id" value="'+ response.currId+ '">\
                            <button class="update-button">✏️</button>\
                            <button class="delete-button">x</button>\
                            <textarea type="text" class="tit" >'+ createTitle.val() + '</textarea>\
                            <textarea type="text" class="name"  " >' + createInput.val() + '</textarea>\
                        </div>\
                    ');
                    createInput.val('');
                    createTitle.val('');
                }
            }
        });
    });

    // UPDATE/PUT
    $('#allMemo').on('click', '.update-button', function () {
        var rowEl = $(this).closest('.memo');
        var id = rowEl.find('.id').val();
        var newName = rowEl.find('.name').val();
        let newTitle = rowEl.find('.tit').val();
        id = Number(id);
        $.ajax({
            url: '/memo/' + id,
            method: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify({ newName: newName, newTitle: newTitle }),
            success: function () {
                console.log(rowEl.find(".memo"));
                rowEl.html('\
                            <input type="text" class="id" value="'+ id + '">\
                            <button class="update-button">✏️</button>\
                            <button class="delete-button">x</button>\
                            <textarea type="text" class="tit" >'+ newTitle + '</textarea>\
                            <textarea type="text" class="name"  " >' + newName + '</textarea>\
            ')
            }
        });
    });



    // DELETE
    $('#allMemo').on('click', '.delete-button', function () {
        var rowEl = $(this).closest('.memo');
        var id = rowEl.find('.id').val();
        id = Number(id);
        $.ajax({
            url: '/memo/' + id,
            method: 'DELETE',
            contentType: 'application/json',
            success: function (response) {
                console.log(response);
                $(rowEl).fadeOut("slow");
                setTimeout("rowEl.remove();", 1000);
            }
        });
    });
   

});
