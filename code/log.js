
const express = require('express');
const https = require('https');
const http = require('http');
const fs = require('fs');
const httpsPort = 3001;
const httpPort=3000;
app = express()

var key = fs.readFileSync(__dirname + '/key.pem');
var cert = fs.readFileSync(__dirname + '/cert.pem');

var credentials = {
    key: key,
    cert: cert
};


var app = express();
var bodyParser = require('body-parser');
const Sequelize = require('sequelize');
/*aaaaaaaaaaaaaaaaaaaaaaaaa*/
var bcrypt = require('bcryptjs');
var nodemailer = require('nodemailer');
require('dotenv').config();
var CryptoJS = require("crypto-js");

let numeroAutenticazione;
/*aaaaaaaaaaaaaaaaaaaaaaaaa*/

/***************************** */
let acc; //nome account globale
let cre; //id account globale

let sequelize = new Sequelize('memoEsame', 'memoEsame', 'memoEsame@2020', {
    host: 'localhost',
    dialect: 'mariadb',
    pool: {
        max: 5,
        min: 0,
        idle: 100
    },
});

const Account = sequelize.import(__dirname + '/models/accounts');
const Book = sequelize.import(__dirname + '/models/books');



Account.hasMany(Book, { onDelete: 'CASCADE' });
Book.belongsTo(Account);

/************************************************* */
sequelize.sync({ force: false }).then(function () {   //

})



/********************** */

app.set('view engine', 'ejs');

var PORT = process.env.PORT || 3000;

app.use(express.static(__dirname));
app.use(express.static(__dirname + '/views'));
app.use(express.urlencoded());
app.use(bodyParser.json());
/******************************************************************* */



app.get('/', function (req, res) {
    acc = "";
    cre = "";
    list = [];
    listSequel = [];
    currentId = 0;
    res.render('index', { esito: "inserire dati " });
});


let fileRdirectLogin="";
let mexLogin="";
app.post('/subm', function (req, res) {
res.render(fileRdirectLogin,{esito:mexLogin})
})

app.post('/login', function (req, res) {
    console.log("login")
    acc = req.body.acc;
    console.log(acc);
    let psw = req.body.enc;
    console.log(psw);
    psw = deCrypting(psw);
    Account.findOne({ where: { email: acc } }).then(account => {
        if (account !== null && `${account.tentativiAccesso}` >= 5) {
            let newPassword = pswCasuale();
            account.update({
                password: cripta(newPassword),
                tentativiAccesso: 0
            });
            sendEmail("Nuova password", "Hanno tentato l'accesso troppe volte al tuo account perciò la password è stata cambiata, la nuova password è: ", newPassword, acc);

            //res.render('index', { esito: "Password sbagliata troppe volte nuova password inviata sull'email" });
            fileRdirectLogin='index';
            mexLogin="Password sbagliata troppe volte nuova password inviata sull'email";
        } else if (account !== null && (bcrypt.compareSync(psw, `${account.password}`))) {
            cre = Number(`${account.id}`);
            Book.findAll({ where: { accountId: cre } }).then(book => {
                book.forEach((customer) => {
                    list.push({ id: Number(`${customer.id}`), name: `${customer.memo}`, title: `${customer.title}` });
                    console.log(`${customer.id}`);
                    console.log(`${customer.memo}`);
                    console.log(`${customer.title}`);
                    currentId++;

                });
            })
            Account.findOne({ where: { email: acc } }).then(account => {
                account.update({

                    tentativiAccesso: 0
                })
            })
            sendEmailAutenticazione();

            //res.render('autenticazioneADueFattori', { esito: "Scrivi il numero che ti è arrivato via mail" });
            fileRdirectLogin='autenticazioneADueFattori';
            mexLogin="Scrivi il numero che ti è arrivato via mail";
        } else {
            Account.findOne({ where: { email: acc } }).then(account12 => {
                account12.update({

                    tentativiAccesso: Number(`${account12.tentativiAccesso}`) + 1
                })
            })
            //res.render('index', { esito: "password o account non validi" });
            fileRdirectLogin='index';
            mexLogin="password o account non validi";
        }
    })
})
app.post('/singup', function (req, res) {
    res.render('singup', { esito: "te" })
})

let fileRdirectCreateAcc="";
let mexCreateAcc="";
app.post('/CreateSubm', function (req, res) {
    res.render(fileRdirectCreateAcc, { esito: mexCreateAcc})
})

app.post('/create', function (req, res) {
    let username = req.body.acc;
    let psw = req.body.enc;
    let psw_repeat = req.body.ripEnc;
    console.log(username)
    console.log(psw) 
    console.log(psw_repeat)
    psw=deCrypting(psw);
    psw_repeat=deCrypting(psw_repeat)
    Account.findOne({ where: { email: username } }).then(accountCreate => {
        if (psw !== psw_repeat) {
            fileRdirectCreateAcc='singup';
            mexCreateAcc='le password non coincidono';
            //res.render('singup', { esito: " le password non coincidono" })
        } else if (accountCreate !== null) {
            fileRdirectCreateAcc='singup';
            mexCreateAcc='Account gia esistente con questa email';
            //res.render('singup', { esito: "Account gia esistente con questa email" })
        } else {

            Account.create(
                { email: username, password: cripta(psw), tentativiAccesso: 0 }
            );
            sendEmail("Creazione account", "é stato creato un nuovo account con questa email sull'app per  i promemoria  ", " ", username);

            fileRdirectCreateAcc='index';
            mexCreateAcc='Account creato correttamente effettuare login';
            //res.render('index', { esito: "Account creato correttamente effettuare login" })
                
        }
    })

})

app.get('/deleteAccount', function (req, res) {
    list = [];
    listSequel = [];
    Account.destroy({
        where: {
            email: acc,
        }
    })
    res.render('index', { esito: "Account cancellato correttamente effettuare login con un altro account o creare nuovo profilo" })
})

/************************************************** */


var list = [];
let listSequel = [];
var currentId = 0;



/********************************************************************* */


app.get('/memo', function (req, res) {
    res.send({ products: list });
});




app.post('/memo', function (req, res) {
    let name = req.body.name;
    let title = req.body.title;
    let accoun = req.body.accoun;
    console.log(title + " deeddeeed")
    cre = accoun;
    console.log(cre + "de");
    if (title === undefined) {
        title = " ";
    }
    listSequel.push({
        memo: name,
        title: title
    });
    /************************************** */


    Book.create(listSequel[listSequel.length - 1]).then(book => {
        Account.findOne({ where: { email: cre } }).then(account1 => {
            account1.addBook(book);
            currentId = `${book.id}`
            console.log(currentId)
            list.push({
                id: Number(currentId),
                name: name,
                title: title
            });
            res.send({ currId: currentId })
        })
    })


    //})

    /**************************************** */


})

app.put('/memo/:id', function (req, res) {
    var id1 = req.params.id;
    var newName = req.body.newName;
    let newTitle = req.body.newTitle;
    var found = false;
    list.forEach(function (product, index) {
        if (!found && product.id == Number(id1)) {//Number id è il :id di memo/:id
            product.name = newName;
            product.title = newTitle;
            console.log(product.title + " title" + product.name + " name")
        }
        console.log(product.title + " title" + product.name + " name" + " id out" + id1 + " ID IN" + product.id)
    });
    /************************* */
    Book.findOne({ where: { id: id1 } }).then(book => {
        book.update({
            memo: newName,
            title: newTitle,
            id: id1
        })
    })
    /********************************* */
    res.send('Succesfully updated product!');
});

app.delete('/memo/:id', function (req, res) {
    var id1 = req.params.id;

    var found = false;

    console.log(list)
    list.forEach(function (product, index) {
        if (!found && product.id === Number(id1)) {
            list.splice(index, 1);
        }

    });
    console.log(list)
    /************************ */
    Book.destroy({
        where: {
            id: Number(id1)
        }
    })
    /*********************** */
    res.send('Successfully deleted product!');
});

/*aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa*/




app.post('/autentica', function (req, res) {
    let autentica = req.body.autentica;
    console.log(autentica);
    console.log(numeroAutenticazione);

    if (autentica == numeroAutenticazione) {
        res.render('memo', { account: acc });
    } else {
        sendEmailAutenticazione();
        res.render('autenticazioneADueFattori', { esito: "CODICE ERRATO REINVIO CODICE" });

    }
})

app.post('/pswDimenticata', function (req, res) {

    let emailChange = req.body.emailMod;
    let newPassword = pswCasuale();

    Account.findOne({ where: { email: emailChange } }).then(account => {
        account.update({

            password: cripta(newPassword)
        })
    })
    sendEmail("Nuova Password", "la nuova password è: ", newPassword, emailChange)
    res.render('index', { esito: "nuova password inviata sull'email" });


})

function cripta(psw) {
    var passwordCriptata = bcrypt.hashSync(psw, bcrypt.genSaltSync(9));
    return passwordCriptata;
}


function sendEmailAutenticazione() {
    // Step 1
    let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: process.env.EMAIL, // TODO: your gmail account
            pass: process.env.PASSWORD // TODO: your gmail password
        }
    });
    console.log(process.env.EMAIL)
    console.log(process.env.PASSWORD)

    numeroAutenticazione = generaNumero() + "";

    // Step 2
    let mailOptions = {
        from: 'verificamemoapp@gmail.com', // TODO: email sender
        to: acc, // TODO: email receiver
        subject: 'Verifica 2 passaggi',
        text: "il codice è: " + numeroAutenticazione
    };

    // Step 3
    transporter.sendMail(mailOptions, (err, data) => {
        if (err) {
            return console.log('Error occurs' + err);
        }
        return console.log('Email sent!!!');
    });
}

function generaNumero() {
    let uno = Math.round(Math.random() * 9);
    let due = Math.round(Math.random() * 9);
    let tre = Math.round(Math.random() * 9);
    let quattro = Math.round(Math.random() * 9);
    let cinque = Math.round(Math.random() * 9);
    let sei = Math.round(Math.random() * 9);

    let numero = uno * 100000 + due * 10000 + tre * 1000 + quattro * 100 + cinque * 10 + sei;
    console.log(numero);
    return numero;
}

function pswCasuale() {
    var length = 9;

    var upper = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var lower = 'abcdefghijklmnopqrstuvwxyz';
    var number = '0123456789';
    var chars = '$&/()!|\><;,:.-_*?^=+[]{}';
    var password = '';
    var seed = upper + lower + number + chars;
    for (var i = 0; i < length; i++) {
        password += seed[Math.floor(Math.random() * (seed.length - 1))];
    }
    return password;



}

function sendEmail(oggetto, testo, newPsw, email) {
    // Step 1
    let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: process.env.EMAIL, // TODO: your gmail account
            pass: process.env.PASSWORD // TODO: your gmail password
        }
    });
    // Step 2
    let mailOptions = {
        from: 'verificamemoapp@gmail.com', // TODO: email sender
        to: email, // TODO: email receiver
        subject: oggetto,
        text: testo + newPsw
    };

    // Step 3
    transporter.sendMail(mailOptions, (err, data) => {
        if (err) {
            return console.log('Error occurs' + err);
        }
        return console.log('Email sent!!!');
    });
}
/*aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa*/
function deCrypting(psw) {
    console.log(process.env.CRYPTING);
    let dec = CryptoJS.AES.decrypt(psw, process.env.CRYPTING);
    console.log(dec);
    stringDec = dec.toString(CryptoJS.enc.Utf8).substr(20);
    console.log(stringDec);
    return stringDec;
}

var httpServer = http.createServer(app);
var httpsServer = https.createServer(credentials, app);

httpServer.listen(httpPort, () => {
    console.log("Http server listing on port : " + httpPort)
});
httpsServer.listen(httpsPort, () => {
    console.log("Https server listing on port : " + httpsPort)
});

